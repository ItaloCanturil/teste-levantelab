<h1 align="center">Teste Levante Lab</h1>

Projeto proposto para a vaga de Front-end Junior na Levante Lab.

## 📑 Descrição do projeto:
O projeto consiste em fazer uma aplicação com autorização de login feito em Node e o Front-End feito em Quasar Framework e VueJS. 
Para fazer mais sentido a aplicação toda, adicionei o "sair da conta".

## :computer: Tecnologias Usadas: 
- Quasar Framework
- NPM Axios
- Node
- VueJS

### 🐾 Comandos para rodar o projeto: 

- Para instalar as dependências:
```
npm install
```
- Iniciar o server: 
```
npm run server
``` 
- Iniciar a aplicação:
```
quasar dev
```

