const express = require('express'),
  bodyParser = require('body-parser'),
  md5 = require('md5'),
  db = require('./database/index'),
  cors = require('cors');

const server = express();

server.use(cors());
server.use(bodyParser.json());

server.post('/api/register', (req, res) => {
  const {
    name,
    email,
    password
  } = req.body;

  db.dataBase.run('INSERT INTO users(name, email, password) VALUES (?, ?, ?)',
    [name, email, md5(password)],
    (error) => {
      if (error) {
        if(error.errno == 19){
          var message = 'Email já cadastrado'
        } else {
          var message = 'Erro ao criar o usuario'
        }
        res.send({
          status: false,
          message: message
        })
      } else {
        res.send({
          status: true,
          message: 'Usuario criado com sucesso'
        })
      }
    })
})

server.post('/api/login', (req, res) => {
  const {
    email,
    password
  } = req.body;

  db.dataBase.get('SELECT name FROM users WHERE email=$email and password=$password', {
    $email: email,
    $password: md5(password)
  }, (error, row) => {
    if(error){
      res.send({
        status: false,
        message: 'Erro ao tentar logar'
      })
    }
    if(row){
      res.send({
        status: true,
        message: 'Usuario existente',
        name: row.name
      })
    }else{
      res.send({
        status: false,
        message: 'Usario não existe'
      })
    }
  })
})

server.listen(5500, () => {
  console.log('server iniciou')
})
