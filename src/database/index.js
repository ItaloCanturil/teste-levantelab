const sqLite = require('sqlite3')

const dataBase = new sqLite.Database('./database.sqlite3', (error) => {
  if(error){
    console.log('erro ao criar o banco de dados', error)
  }else {
    console.log('criado com sucesso')
  }
});

function createTable() {
  dataBase.run('CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, email TEXT UNIQUE, password TEXT)')
  console.log('table created')
};

createTable();

exports.dataBase = dataBase;