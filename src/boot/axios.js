import Vue from 'vue'
import axios from 'axios'

const baseUrl = 'http://127.0.0.1:5500/'
const axiosInstance = axios.create({
  baseURL: baseUrl,
  headers: {
    'X-Requested-With': 'XMLHttpRequest'
  }
})

axiosInstance.interceptors.request.use(config => {
  const token = sessionStorage.getItem('token')
  if (token) {
    config.headers.Authorization = `Bearer ${token}`
  }

  return config
})

Vue.prototype.$axios = axiosInstance
