
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'login', component: () => import('pages/auth/Login.vue') },
      { path: 'register', component: () => import('pages/auth/Register.vue') }
    ]
  },
  {
    path: '/profile',
    component: () => import('layouts/Profile.vue'),
    children: [
      { path: '', component: () => import('pages/profile/Index.vue') },
      { path: 'gallery', component: () => import('pages/profile/Gallery.vue') },
      { path: 'dashboard', component: () => import('pages/profile/Dashboard.vue') },
      { path: 'following', component: () => import('pages/profile/Following.vue') },
      { path: 'video', component: () => import('pages/profile/VideoDetail.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
